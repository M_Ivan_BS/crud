<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CRUD en PHP y MySQL</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="https://kit.fontawesome.com/177e0a8a90.js" crossorigin="anonymous"></script>
</head>
<body>
    <script>
        function eliminar(){
            var respuesta=confirm("Estas seguro de que deseas eliminar?");
            return respuesta
        }
    </script>
    <div class="container-fluid row">
        <form class="col-4 p-3" method="POST">
            <h3 class="text-center text-secondary">Registro</h3>
            <?php
            include "modelo/conexion.php";
            include "controlador/registro_cliente.php";
            include "controlador/eliminar_cliente.php";
            ?>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Nombre</label>
                <input type="text" class="form-control" name="nombre">
            </div>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Edad:</label>
                <input type="text" class="form-control" name="edad">
            </div>

            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Telefono</label>
                <input type="text" class="form-control" name="telefono">
            </div>
            <button type="submit" class="btn btn-primary" name="btnregistar" value="ok">Registrar</button>
        </form>

        <div class="col-8 p-4">
            <table class="table">
                <thead class="bg-info">
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Edad</th>
                    <th scope="col">Telefono</th>
                    <th scope="col">Eliminar | Modificar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    include "modelo/conexion.php";
                    $sql=$conexion->query(" SELECT * FROM clientes ");
                    while($datos=$sql->fetch_object()){ ?>
                    <tr>
                        <td><?= $datos->id ?></td>
                        <td><?= $datos->nombre ?></td>
                        <td><?= $datos->edad ?></td>
                        <td><?= $datos->telefono ?></td>
                        <td>
                            <a onclick="return eliminar()" href="index.php?id=<?= $datos->id ?>" class="btn btn-small btn-warning px-4 ms-1"><i class="fa-solid fa-trash"></i></a>
                            <a href="modificar_cliente.php?id=<?= $datos->id ?>" class="btn btn-small btn-danger px-4"><i class="fa-solid fa-pen-to-square"></i></a>
                        </td>
                    </tr>

                    <?php }
                    ?>
                    
                </tbody>
            </table>
        </div>
        
    </div>
   
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</body>
</html>